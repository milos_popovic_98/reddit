graph [
  directed 1
  node [
    id 0
    label "netsec"
  ]
  node [
    id 1
    label "AskReddit"
  ]
  node [
    id 2
    label "environment"
  ]
  node [
    id 3
    label "ideas"
  ]
  node [
    id 4
    label "obama"
  ]
  node [
    id 5
    label "programming"
  ]
  node [
    id 6
    label "entertainment"
  ]
  node [
    id 7
    label "pics"
  ]
  node [
    id 8
    label "philosophy"
  ]
  node [
    id 9
    label "atheism"
  ]
  node [
    id 10
    label "WTF"
  ]
  node [
    id 11
    label "science"
  ]
  node [
    id 12
    label "business"
  ]
  node [
    id 13
    label "nsfw"
  ]
  node [
    id 14
    label "comics"
  ]
  node [
    id 15
    label "cogsci"
  ]
  node [
    id 16
    label "Health"
  ]
  node [
    id 17
    label "geek"
  ]
  node [
    id 18
    label "technology"
  ]
  node [
    id 19
    label "photography"
  ]
  node [
    id 20
    label "canada"
  ]
  node [
    id 21
    label "Economics"
  ]
  node [
    id 22
    label "math"
  ]
  node [
    id 23
    label "politics"
  ]
  node [
    id 24
    label "funny"
  ]
  node [
    id 25
    label "videos"
  ]
  node [
    id 26
    label "bestof"
  ]
  node [
    id 27
    label "scifi"
  ]
  node [
    id 28
    label "linux"
  ]
  node [
    id 29
    label "gaming"
  ]
  node [
    id 30
    label "joel"
  ]
  node [
    id 31
    label "reddit.com"
  ]
  node [
    id 32
    label "news"
  ]
  node [
    id 33
    label "software"
  ]
  node [
    id 34
    label "worldnews"
  ]
  node [
    id 35
    label "offbeat"
  ]
  node [
    id 36
    label "history"
  ]
  node [
    id 37
    label "gadgets"
  ]
  node [
    id 38
    label "guns"
  ]
  edge [
    source 0
    target 23
    weight 0.09056973375312725
  ]
  edge [
    source 0
    target 5
    weight 0.10426858994507206
  ]
  edge [
    source 0
    target 31
    weight 0.22959304478562698
  ]
  edge [
    source 0
    target 11
    weight 0.046568493184321
  ]
  edge [
    source 0
    target 18
    weight 0.0479478687194496
  ]
  edge [
    source 1
    target 7
    weight 0.05459571294611202
  ]
  edge [
    source 1
    target 23
    weight 0.0940704540311546
  ]
  edge [
    source 1
    target 31
    weight 0.1869841780016598
  ]
  edge [
    source 2
    target 23
    weight 0.13974352367251547
  ]
  edge [
    source 2
    target 31
    weight 0.19248553438878582
  ]
  edge [
    source 2
    target 11
    weight 0.056178025220084256
  ]
  edge [
    source 3
    target 1
    weight 0.04700720121719538
  ]
  edge [
    source 3
    target 23
    weight 0.10791118377381523
  ]
  edge [
    source 3
    target 31
    weight 0.19711372650761588
  ]
  edge [
    source 3
    target 11
    weight 0.0558905473475438
  ]
  edge [
    source 4
    target 7
    weight 0.04554023322785611
  ]
  edge [
    source 4
    target 23
    weight 0.24110813242821139
  ]
  edge [
    source 4
    target 31
    weight 0.18498044708941705
  ]
  edge [
    source 5
    target 23
    weight 0.07107833630138387
  ]
  edge [
    source 5
    target 31
    weight 0.15478145592497458
  ]
  edge [
    source 6
    target 7
    weight 0.06143800185042715
  ]
  edge [
    source 6
    target 23
    weight 0.10737075199283391
  ]
  edge [
    source 6
    target 31
    weight 0.22279771114902594
  ]
  edge [
    source 7
    target 10
    weight 0.04952637593626982
  ]
  edge [
    source 7
    target 24
    weight 0.05281852118327834
  ]
  edge [
    source 7
    target 23
    weight 0.11471315700996336
  ]
  edge [
    source 7
    target 31
    weight 0.22005700571186185
  ]
  edge [
    source 8
    target 23
    weight 0.0989673671701588
  ]
  edge [
    source 8
    target 5
    weight 0.05438149513916553
  ]
  edge [
    source 8
    target 31
    weight 0.16844876230606606
  ]
  edge [
    source 8
    target 11
    weight 0.059718928183150964
  ]
  edge [
    source 9
    target 7
    weight 0.04817862144920815
  ]
  edge [
    source 9
    target 23
    weight 0.134856522623412
  ]
  edge [
    source 9
    target 31
    weight 0.18573287633876434
  ]
  edge [
    source 9
    target 11
    weight 0.04887976017694484
  ]
  edge [
    source 10
    target 24
    weight 0.05258071471595435
  ]
  edge [
    source 10
    target 7
    weight 0.0776243890843493
  ]
  edge [
    source 10
    target 23
    weight 0.12549396598874235
  ]
  edge [
    source 10
    target 31
    weight 0.21007816056281456
  ]
  edge [
    source 11
    target 7
    weight 0.05557455983025845
  ]
  edge [
    source 11
    target 23
    weight 0.13307793530537473
  ]
  edge [
    source 11
    target 5
    weight 0.05430303306559942
  ]
  edge [
    source 11
    target 31
    weight 0.2295793061994648
  ]
  edge [
    source 11
    target 34
    weight 0.04825454312933595
  ]
  edge [
    source 12
    target 23
    weight 0.15166634335396104
  ]
  edge [
    source 12
    target 31
    weight 0.22766750379367587
  ]
  edge [
    source 12
    target 34
    weight 0.04966547813727031
  ]
  edge [
    source 13
    target 7
    weight 0.07452592148449592
  ]
  edge [
    source 13
    target 23
    weight 0.08598132274573281
  ]
  edge [
    source 13
    target 31
    weight 0.17844809464118427
  ]
  edge [
    source 14
    target 10
    weight 0.04552616626671035
  ]
  edge [
    source 14
    target 24
    weight 0.04936768070866318
  ]
  edge [
    source 14
    target 7
    weight 0.07102783865407843
  ]
  edge [
    source 14
    target 23
    weight 0.10154668117530434
  ]
  edge [
    source 14
    target 31
    weight 0.18587689112823566
  ]
  edge [
    source 15
    target 23
    weight 0.1033621236284323
  ]
  edge [
    source 15
    target 5
    weight 0.06077561462681213
  ]
  edge [
    source 15
    target 31
    weight 0.1917852190098221
  ]
  edge [
    source 15
    target 11
    weight 0.07339872996012581
  ]
  edge [
    source 16
    target 23
    weight 0.11352679844382471
  ]
  edge [
    source 16
    target 31
    weight 0.17229496457501778
  ]
  edge [
    source 16
    target 11
    weight 0.05692179581466508
  ]
  edge [
    source 17
    target 7
    weight 0.06014254651313834
  ]
  edge [
    source 17
    target 23
    weight 0.08684348072112699
  ]
  edge [
    source 17
    target 5
    weight 0.05874960074129944
  ]
  edge [
    source 17
    target 31
    weight 0.18899829075094227
  ]
  edge [
    source 17
    target 11
    weight 0.045969630762834786
  ]
  edge [
    source 18
    target 7
    weight 0.05279312182866104
  ]
  edge [
    source 18
    target 23
    weight 0.10980722470589417
  ]
  edge [
    source 18
    target 5
    weight 0.060721193752357364
  ]
  edge [
    source 18
    target 31
    weight 0.21734083399575677
  ]
  edge [
    source 18
    target 11
    weight 0.048896616009357255
  ]
  edge [
    source 19
    target 7
    weight 0.06946005774849798
  ]
  edge [
    source 19
    target 23
    weight 0.08143103854023089
  ]
  edge [
    source 19
    target 5
    weight 0.04995048654798003
  ]
  edge [
    source 19
    target 31
    weight 0.17671882084854298
  ]
  edge [
    source 20
    target 23
    weight 0.09602987923528229
  ]
  edge [
    source 20
    target 31
    weight 0.18229398363583457
  ]
  edge [
    source 20
    target 11
    weight 0.04723083202018426
  ]
  edge [
    source 20
    target 34
    weight 0.0538602371705766
  ]
  edge [
    source 21
    target 12
    weight 0.06832510101287301
  ]
  edge [
    source 21
    target 23
    weight 0.1772268199643132
  ]
  edge [
    source 21
    target 5
    weight 0.046283781236158915
  ]
  edge [
    source 21
    target 31
    weight 0.17886606833392357
  ]
  edge [
    source 21
    target 34
    weight 0.05234370119474212
  ]
  edge [
    source 22
    target 23
    weight 0.0786667270728816
  ]
  edge [
    source 22
    target 5
    weight 0.1443536429660869
  ]
  edge [
    source 22
    target 31
    weight 0.1861127774889747
  ]
  edge [
    source 22
    target 11
    weight 0.0784107128521659
  ]
  edge [
    source 23
    target 31
    weight 0.2233247118261568
  ]
  edge [
    source 23
    target 34
    weight 0.04784290739659797
  ]
  edge [
    source 24
    target 10
    weight 0.05262882722221397
  ]
  edge [
    source 24
    target 7
    weight 0.08286003077025024
  ]
  edge [
    source 24
    target 23
    weight 0.11998294961192689
  ]
  edge [
    source 24
    target 31
    weight 0.22004333633959905
  ]
  edge [
    source 25
    target 10
    weight 0.048426926242219254
  ]
  edge [
    source 25
    target 24
    weight 0.0454969180958565
  ]
  edge [
    source 25
    target 7
    weight 0.07677859693407317
  ]
  edge [
    source 25
    target 23
    weight 0.10944964855632185
  ]
  edge [
    source 25
    target 31
    weight 0.18313693014579083
  ]
  edge [
    source 26
    target 1
    weight 0.06346862554549064
  ]
  edge [
    source 26
    target 7
    weight 0.059648317601859575
  ]
  edge [
    source 26
    target 23
    weight 0.09864119029927124
  ]
  edge [
    source 26
    target 5
    weight 0.0456996946497319
  ]
  edge [
    source 26
    target 31
    weight 0.20222671680599974
  ]
  edge [
    source 27
    target 7
    weight 0.04745343883610667
  ]
  edge [
    source 27
    target 23
    weight 0.10744559603095406
  ]
  edge [
    source 27
    target 5
    weight 0.053904090911149755
  ]
  edge [
    source 27
    target 31
    weight 0.19896302872897037
  ]
  edge [
    source 27
    target 11
    weight 0.05656015582523878
  ]
  edge [
    source 28
    target 23
    weight 0.07420127940009101
  ]
  edge [
    source 28
    target 5
    weight 0.13580273352748976
  ]
  edge [
    source 28
    target 31
    weight 0.16615237634635616
  ]
  edge [
    source 28
    target 18
    weight 0.05600787615520817
  ]
  edge [
    source 29
    target 7
    weight 0.05520093086717651
  ]
  edge [
    source 29
    target 23
    weight 0.08967027082895575
  ]
  edge [
    source 29
    target 5
    weight 0.055495516817760264
  ]
  edge [
    source 29
    target 31
    weight 0.19056070069513206
  ]
  edge [
    source 30
    target 5
    weight 0.09610139889352567
  ]
  edge [
    source 30
    target 31
    weight 0.12968621119935123
  ]
  edge [
    source 31
    target 23
    weight 0.09971248226817289
  ]
  edge [
    source 32
    target 23
    weight 0.17869619105337287
  ]
  edge [
    source 32
    target 31
    weight 0.1706090702331163
  ]
  edge [
    source 32
    target 34
    weight 0.05783100877446067
  ]
  edge [
    source 33
    target 23
    weight 0.06814680919766668
  ]
  edge [
    source 33
    target 5
    weight 0.08625834181929534
  ]
  edge [
    source 33
    target 31
    weight 0.18081182603756132
  ]
  edge [
    source 33
    target 18
    weight 0.06315664248072546
  ]
  edge [
    source 34
    target 7
    weight 0.05090253709195964
  ]
  edge [
    source 34
    target 23
    weight 0.1678050778805167
  ]
  edge [
    source 34
    target 31
    weight 0.2181843605806874
  ]
  edge [
    source 34
    target 11
    weight 0.048056349164512895
  ]
  edge [
    source 35
    target 10
    weight 0.053705289364154786
  ]
  edge [
    source 35
    target 24
    weight 0.04550139616285965
  ]
  edge [
    source 35
    target 7
    weight 0.07102584286532526
  ]
  edge [
    source 35
    target 23
    weight 0.10940406232368653
  ]
  edge [
    source 35
    target 31
    weight 0.20244370345804558
  ]
  edge [
    source 36
    target 23
    weight 0.1609676344398208
  ]
  edge [
    source 36
    target 31
    weight 0.16514132820860591
  ]
  edge [
    source 36
    target 34
    weight 0.07265321231936794
  ]
  edge [
    source 37
    target 7
    weight 0.05412432642246075
  ]
  edge [
    source 37
    target 23
    weight 0.08332515548451748
  ]
  edge [
    source 37
    target 31
    weight 0.21860114512050755
  ]
  edge [
    source 37
    target 11
    weight 0.04791219342446773
  ]
  edge [
    source 37
    target 18
    weight 0.05322870980916218
  ]
  edge [
    source 38
    target 23
    weight 0.13157608273975316
  ]
  edge [
    source 38
    target 31
    weight 0.26561909215634477
  ]
]
