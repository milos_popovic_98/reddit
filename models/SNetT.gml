graph [
  directed 1
  node [
    id 0
    label "netsec"
  ]
  node [
    id 1
    label "ideas"
  ]
  node [
    id 2
    label "AskReddit"
  ]
  node [
    id 3
    label "environment"
  ]
  node [
    id 4
    label "obama"
  ]
  node [
    id 5
    label "programming"
  ]
  node [
    id 6
    label "entertainment"
  ]
  node [
    id 7
    label "pics"
  ]
  node [
    id 8
    label "philosophy"
  ]
  node [
    id 9
    label "atheism"
  ]
  node [
    id 10
    label "WTF"
  ]
  node [
    id 11
    label "science"
  ]
  node [
    id 12
    label "business"
  ]
  node [
    id 13
    label "nsfw"
  ]
  node [
    id 14
    label "comics"
  ]
  node [
    id 15
    label "cogsci"
  ]
  node [
    id 16
    label "Health"
  ]
  node [
    id 17
    label "geek"
  ]
  node [
    id 18
    label "technology"
  ]
  node [
    id 19
    label "photography"
  ]
  node [
    id 20
    label "canada"
  ]
  node [
    id 21
    label "Economics"
  ]
  node [
    id 22
    label "math"
  ]
  node [
    id 23
    label "politics"
  ]
  node [
    id 24
    label "funny"
  ]
  node [
    id 25
    label "videos"
  ]
  node [
    id 26
    label "bestof"
  ]
  node [
    id 27
    label "scifi"
  ]
  node [
    id 28
    label "linux"
  ]
  node [
    id 29
    label "gaming"
  ]
  node [
    id 30
    label "joel"
  ]
  node [
    id 31
    label "reddit.com"
  ]
  node [
    id 32
    label "news"
  ]
  node [
    id 33
    label "software"
  ]
  node [
    id 34
    label "worldnews"
  ]
  node [
    id 35
    label "offbeat"
  ]
  node [
    id 36
    label "history"
  ]
  node [
    id 37
    label "gadgets"
  ]
  node [
    id 38
    label "guns"
  ]
  edge [
    source 0
    target 31
    weight 0.22959304478562698
  ]
  edge [
    source 1
    target 31
    weight 0.19711372650761588
  ]
  edge [
    source 4
    target 23
    weight 0.24110813242821139
  ]
  edge [
    source 6
    target 31
    weight 0.22279771114902594
  ]
  edge [
    source 7
    target 31
    weight 0.22005700571186185
  ]
  edge [
    source 10
    target 31
    weight 0.21007816056281456
  ]
  edge [
    source 11
    target 31
    weight 0.2295793061994648
  ]
  edge [
    source 12
    target 31
    weight 0.22766750379367587
  ]
  edge [
    source 18
    target 31
    weight 0.21734083399575677
  ]
  edge [
    source 23
    target 31
    weight 0.2233247118261568
  ]
  edge [
    source 24
    target 31
    weight 0.22004333633959905
  ]
  edge [
    source 26
    target 31
    weight 0.20222671680599974
  ]
  edge [
    source 27
    target 31
    weight 0.19896302872897037
  ]
  edge [
    source 34
    target 31
    weight 0.2181843605806874
  ]
  edge [
    source 35
    target 31
    weight 0.20244370345804558
  ]
  edge [
    source 37
    target 31
    weight 0.21860114512050755
  ]
  edge [
    source 38
    target 31
    weight 0.26561909215634477
  ]
]
